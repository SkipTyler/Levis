// мобильное меню
var menuBtn = document.querySelector(".js-menu-btn"),
	menu = document.querySelector(".menu"),
	menuLink = menu.getElementsByTagName("a");

function toggleMenu() {
	menu.classList.toggle("active");
	menuBtn.classList.toggle("active");
}

menuBtn.onclick = function(event) {
	event.preventDefault();
	toggleMenu();
};

for (var i = 0; i < menuLink.length; i++) {
	menuLink[i].addEventListener("click", function() {
		menu.classList.remove("active");
		menuBtn.classList.remove("active");
	});
}


// слайдер товаров
$('.catalog-tab-slider-item').slick({
	arrows: false,
	fade: true,
	asNavFor: '.catalog-tab-slider-nav'
});

$('.catalog-tab-slider-nav').slick({
	slidesToShow: 3,
	slidesToScroll: 1,
	arrows: false,
	asNavFor: '.catalog-tab-slider-item',
	centerMode: true,
	focusOnSelect: true,
	variableWidth: true
});

// слайдер отзывов
$('.testimonials__slider').slick({
	dots: true,
	dotsClass: 'js-slick-dots',
	prevArrow: '<button class="js-arrow leftArrow"></button>',
	nextArrow: '<button class="js-arrow rightArrow"></button>',
	responsive: [{
			breakpoint: 620,
			settings: {
				arrows: false
			}
		}]
});


// фенсик галлерея
$('[data-fancybox]').fancybox({});


// табулятор для каталога
$(function () {

	$(".catalog__item").on("click", function () {

		$('.catalog-tab').removeClass("catalog-tab-active");

		var id = $(this).data("id");
		$("#catalog-tab" + id).addClass("catalog-tab-active");
		$("html, body").animate({ scrollTop: $("#catalog-tab" + id).offset().top }, 1000);
	});


});

// Кнопка вверх и стили меню

$(function () {
	var $btnTop = $(".btn-top"),
		$fixMenu = $(".header__menu");
	$(window).on("scroll", function() {
		if ($(window).scrollTop() >= 150) {
			$fixMenu.addClass("active");
			$btnTop.fadeIn();
		} else {
			$fixMenu.removeClass("active");
			$btnTop.fadeOut();
		}
	});

	$btnTop.on("click", function(e) {
		e.preventDefault();
		$("html,body").animate({ scrollTop: 0 }, 900)
	});});

// плавная прокрутка
$(".menu").on("click","a", function (event) {
	event.preventDefault();
	var id  = $(this).attr('href'),
		top = $(id).offset().top;
	$('body,html').animate({scrollTop: top}, 900);
});


// модальные окна
$(function() {
	// новая модака
	var modal = {
		self: $(".modal"),

		showModal: function(content) {
			this.self.find("#innerModal").html(content);
			this.self.fadeIn(500);
		},
		hideModal: function() {
			this.self.fadeOut(200);
			this.self.find("#innerModal").html("");
		}
	};

	//показать модал
	$(".js-showModal").on("click", function(e) {
		e.preventDefault();
		var id = $(this).data("id");
		var content = $("#cont" + id).html();
		modal.showModal(content);
	});

	//скрыть модал
	$(".modal").on("click", function(e) {
		e.preventDefault();
		if (
			$(e.target).attr("id") === "modal" ||
			$(e.target).hasClass("js-close-modal")
		) {
			modal.hideModal();
		} else {
			return false;
		}
	});
});

$(".advantages__guide").find('a').on('click', function (e) {
	e.preventDefault();
});
